import relationalStore from '@ohos.data.relationalStore'
import valueBuckets from './rdbData'
import chatValueBuckets from './chatData'
import momentData from './momentData'
const TAG = "tencent"


class ReadRdb {
    private rdbStore: relationalStore.RdbStore = undefined
    private STORE_CONFIG = { name: "RdbTest1.db", securityLevel: relationalStore.SecurityLevel.S1 }
    private chatData = []
    private chatContent = []

    async getRdbStore() {
        this.rdbStore = await relationalStore.getRdbStore(globalThis.context, this.STORE_CONFIG);
        //console.log(TAG,"Get RdbStore successfully.")
        const SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS CHATLIST (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, AGE INTEGER, ME TEXT NOT NULL, TIME TEXT NOT NULL,SALARY REAL, CODES BLOB)"
        const SQL_CREATE_TABLE1 = "CREATE TABLE IF NOT EXISTS CHATDETAIL (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, AGE INTEGER, SALARY REAL, CODES BLOB)"
        const SQL_CREATE_TABLE2 = "CREATE TABLE IF NOT EXISTS MOMENTDATA (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, IMG INTEGER, ME TEXT NOT NULL, SALARY REAL, CODES BLOB)"
        try {
            await this.rdbStore.executeSql(SQL_CREATE_TABLE)
            console.log(TAG, "GET CHATLIST successfully.")
        } catch (e) {
            console.error(TAG, 'getRdbStore111:' + JSON.stringify(e))
        }
        try {
            await this.rdbStore.executeSql(SQL_CREATE_TABLE1)
            console.log(TAG, "GET CHATDETAIL successfully.")
        } catch (e) {
            console.error(TAG, 'getRdbStore222:' + JSON.stringify(e))
        }
        try {
            await this.rdbStore.executeSql(SQL_CREATE_TABLE2)
            console.log(TAG, "GET CHATDETAIL successfully.")
        } catch (e) {
            console.error(TAG, 'getRdbStore222:' + JSON.stringify(e))
        }
    }
    //插入聊天列表数据
    async insertChatListData() {
        let insert = await this.rdbStore.batchInsert("CHATLIST", valueBuckets)
        console.log(TAG, `insert CHATLIST donw`)
    }
    //查询聊天列表数据
    async queryChatList() {
        let predicates = new relationalStore.RdbPredicates("CHATLIST")
        predicates.isNotNull("NAME")
        let resultSet = await this.rdbStore.query(predicates, [])
        resultSet.goToFirstRow()
        let isLast = true
        while (isLast) {
            let name = resultSet.getString(1)
            let img = resultSet.getString(2)
            let news = resultSet.getString(3)
            let time = resultSet.getString(4)
            let arr = [name, img, news, time]
            this.chatData.push(arr)
            isLast = resultSet.goToNextRow()
        }
        if (!isLast) {
            resultSet.close()
        }
        return this.chatData
    }

    //插入聊天详情数据
    async insertChatDetailData() {
        let insert = await this.rdbStore.batchInsert("CHATDETAIL", chatValueBuckets);
        console.log(TAG, `insert CHATDETAIL done`)
    }
    //查询聊天详情数据
    async queryChatDetail() {
        let predicates = new relationalStore.RdbPredicates("CHATDETAIL")
        predicates.isNotNull("NAME")
        let resultSet = await this.rdbStore.query(predicates, [])
        resultSet.goToFirstRow()
        let isLast = true
        while (isLast) {
            let name = resultSet.getString(1)
            this.chatContent.push(name)
            isLast = resultSet.goToNextRow()
            // console.log(TAG, "chatdata" + name)
            // console.log(TAG, "chatdatadone")
            // console.log(TAG, "chatdatalast" + isLast)
        }
        if (!isLast) {
            resultSet.close()
        }
        return this.chatData
    }
    //插入朋友圈数据
    async insertMomentData() {
        let insert = await this.rdbStore.batchInsert("MOMENTDATA", momentData)
        console.log(TAG, `insert MOMENTDATA done`);
    }
    //查询朋友圈数据
    async queryMomentData(start: number, end: number) {
        let momentData = []
        let predicates = new relationalStore.RdbPredicates("MOMENTDATA")
        predicates.between("ID", start, end)
        let resultSet = await this.rdbStore.query(predicates, [])
        resultSet.goToFirstRow()
        let isLast = true
        while (isLast) {
            let id = resultSet.getString(0)
            let name = resultSet.getString(1)
            let head = resultSet.getString(2)
            let text = resultSet.getString(3)
            let momentItem = [id, name, head, text]
            this.chatData.push(momentItem)
            isLast = resultSet.goToNextRow()
        }
        if (!isLast) {
            resultSet.close()
        }
        return momentData
    }
}

export default new ReadRdb()