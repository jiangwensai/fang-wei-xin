import data_preferences from '@ohos.data.preferences'
import worker from '@ohos.worker';
const TAG = 'tencent'
let preferenceThem: data_preferences.Preferences = null
const PREFERENCES_NAME = 'theme.db'

export class SwitchControlClass {
  async getPreferencesFromStorage() {
    let context = getContext(this) as any
    preferenceThem = await data_preferences.getPreferences(context, PREFERENCES_NAME)
  }

  async putPreference(key:string, data:string){
    console.log(TAG, 'PUT BEGIN');
    if(preferenceThem !== null) {
      await preferenceThem.put(key,data)
      await preferenceThem.flush()
    }
  }

  async getPreference(key:string){
    console.log(TAG, 'get BEGIN');
    let theme:string = ''
    if(preferenceThem !== null) {
      theme = <string> await preferenceThem.get(key, 'default')
      console.log(TAG,"fiveThousandBtn111"+theme)
      return theme
    }
  }
}

class TransmissionFileWorker {
  private transmissionFileWorkerObj = []
  private sto = []

  start(time) {
    this.transmissionFileWorker({method: 'get',time})
    this.transmissionFileWorker({method: 'post',time})
  }

  end() {
    for (let index = 0; index < 2; index++){
      clearInterval(this.sto[index])
      this.transmissionFileWorkerObj[index].terminate()
    }
  }

  transmissionFileWorker({method,time}){
    let index = method === 'get' ? 0 : 1
    if (method == 'get'){
      this.transmissionFileWorkerObj[index] = new worker.ThreadWorker('entrt/ets/workers/GetFiles.ts')
    } else if(method === "post") {
      this.transmissionFileWorkerObj[index] = new worker.ThreadWorker('entrt/ets/workers/PostFiles.ts')
    }

    this.sto[index] = setInterval(()=>{
      this.transmissionFileWorkerObj[index].postMessage(1)
    },time)

    this.transmissionFileWorkerObj[index].onmessage = (d) => {
      var data = d.data
      console.log(TAG,`线程${method}回复的数据为11111 ：` + JSON.stringify(data))
    }
  }
}
export default new TransmissionFileWorker()