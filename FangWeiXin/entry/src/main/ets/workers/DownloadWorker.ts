import work from '@ohos.worker'
import http from '@ohos.net.http'
import fs from '@ohos.file.fs'
const parentPort = worker.workerPort
const TAG = 'tencent'

parentPort.onmessage = (e) => {
    let urlArr = e.data.urlArr
    // console.log(TAG + "before download")
    for (let i = 0; i < urlArr.length; i++) {
        let url = urlArr[i]
        let httpRequest = http.createHttp()
        // console.log(TAG + 'downloadStart')
        httpRequest.request(url, {
            method: http.RequestMethod.GET,
            header: {
                'Content-Type': 'application/json'
            },
            expectDataType: http.HttpDataType.ARRAY_BUFFER,
            connectTimeout: 10000, // 可选，默认为60000ms
            readTimeout: 10000, // 可选，默认为60000ms
        }).then((data) => {
            // console.log(TAG, 'requestStart')
            let result = data.result as ArrayBuffer
            let fileName = "image_" + i + ".jpg"
            let path = e.data.savePath + fileName
            // console.log(TAG + "path is: " + path)
            let fd = fs.openSync(path, 0o2 | 0o100)
            let stream = fs.fdopenStreamSync(fd.fd, 'r+')
            let downLoadResult = stream.writeSync(result)
            stream.close().then(() => {
                fs.close(fd).then(() => {

                })
            })
            // console.log(TAG, 'downloadResult' + downLoadResult)
            httpRequest.destroy();
            parentPort.postMessage({
                index: i,
                downloadSuccess: downLoadResult,
            })
        }).catch((err) => {
            parentPort.postMessage({
                index: i,
                downloadSuccess: false,
                fileName: "image_" + i + ".jpg"
            })
        })
    }
}